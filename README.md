SoftwareSerialLite , derivated from SoftwareSerial (formerly NewSoftSerial)

This is a lite version from SoftwareSerial, with only the basic, Serial
comunication between two arduinos by software. 

It consumes about 300bytes less of program code than original. 

You can use the original one if you not define LITE in definitions part.


Some functions removed too in this lite version like listen() and stopListening()
because you can use only one serial comunication, end(), and peek(), and inverse logic,
But they are in code, you can tweak it to include or not.

This modification is intended for small scenarios where space is important

If anyone knows how to lite more this library, you can help us

Original code: https://github.com/arduino/Arduino/tree/master/hardware/arduino/avr/libraries/SoftwareSerial